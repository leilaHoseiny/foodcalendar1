import {Component} from '@angular/core';
import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {FoodService} from './api/food.service';
import {Push, PushObject, PushOptions} from '@ionic-native/push/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  // private data: any;
  constructor(private platform: Platform,
              private splashScreen: SplashScreen,
              private statusBar: StatusBar,
              public push: Push,
              public api: FoodService,
           ){
    this.initializeApp();
    // this.changeTheme();
  }

  // changeTheme()
  // {
  //   // this.api.getApiFood();
  // }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      let options: PushOptions = {
        android: {},
        ios: {alert: 'true', badge: true, sound: 'false'},
        windows: {},
      };

      const pushObject: PushObject = this.push.init(options);

      pushObject.on('notification').subscribe((notification: any) =>
        // notification.message,
        // notification.title,
        // notification.count,
        // notification.sound,
        // notification.image,
        // notification.additionalData
        console.log('recieve a notification', notification)
      );

      pushObject.on('registration').subscribe((data)=>{

        //data.registrationId
        console.log('notification registeration id: ' + data);

        let dataI = {
          type : 'windows',
          registration_id : data,
          active : true
        };

        if(this.platform.is('ios'))
        {
          dataI.type ='ios';
        } else if(this.platform.is('android'))
        {
          dataI.type ='android';
        }

        this.api.postNotification(dataI).subscribe((res:any)=>{
          console.log(res);
        });

      });

      pushObject.on('error').subscribe((e)=> {
        // e.message
        console.log('error notification',e);
      });
    });
  }
}
