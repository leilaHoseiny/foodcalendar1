//<reference path="../../node_modules/@ionic-native/push/index.d.ts"/>
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Camera } from '@ionic-native/camera/ngx';
import { IonicStorageModule } from '@ionic/storage';
import {HttpClientModule} from '@angular/common/http';
import {FoodService} from './api/food.service';
import {DpDatePickerModule} from 'ng2-jalali-date-picker';
import { NgJalaliCalendarModule } from 'ng-jalali-calendar'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Push } from '@ionic-native/push/ngx';
import { File } from '@ionic-native/file/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
    IonicStorageModule.forRoot(),
      HttpClientModule,
      DpDatePickerModule,
      NgJalaliCalendarModule,
     BrowserAnimationsModule,
  ],
  providers: [
    StatusBar,
    SocialSharing,
    File,
    SplashScreen,
    Camera,
    FoodService,
    Push,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
