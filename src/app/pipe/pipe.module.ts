import { NgModule } from '@angular/core';
import { FiltrPipe } from './filtr.pipe';
// import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [FiltrPipe],
    exports:[FiltrPipe],
  imports: []
})
export class PipeModule { }
