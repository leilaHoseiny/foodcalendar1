import { Component, OnInit } from '@angular/core';
import {FoodService} from '../../api/food.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  public items:any;

  constructor(private api:FoodService) {this.getDetails(); }

  ngOnInit() {
  }

  getDetails()
  {
    this.api.getAbout().subscribe((res:any)=>{

       this.items = res.content;
       console.log(res.content);
    });
  }
}
