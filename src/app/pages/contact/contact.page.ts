import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertController, LoadingController, ToastController} from '@ionic/angular';
import {FoodService} from '../../api/food.service';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  public contactForm: FormGroup;
  public spinner:boolean =true;
  massage: any;
  errorMassage: any;

    constructor(public formBuilder: FormBuilder,
                private api:FoodService,
                private alertControl: AlertController,
                private toastControl: ToastController,
                public loadingControl: LoadingController) {
        this.contactForm = this.formBuilder.group({
            name: ["", [Validators.required, Validators.maxLength(20)]],
            email: ["", [Validators.required, Validators.email]],
            message: ["", Validators.required],
        });
    }

    ngOnInit() {}

    async presentAlert() {

        const alert = await this.alertControl.create({
            // header: 'Alert',
            // subHeader: 'Subtitle',
            message: this.massage,
            buttons: ['OK']
        });
         // noinspection TypeScriptUnresolvedFunction
        await alert.present();
    }

  async presentLoading() {
   let loading = await this.loadingControl.create({
      message: 'لطفا منتظر بمانید...', // For me 'content' does not exist, it's 'message' instead
    });

    // noinspection TypeScriptUnresolvedFunction
    return await loading.present();
  }

    addContact()
    {
      this.presentLoading();
        this.spinner=false;
        let value=this.contactForm.value;
        console.log(value);
        let newValue : any = {
            email: value.email,
            name: value.name,
            message: value.message
        };
        // const loading = this.loadingControl.create(this.loadingConfig);
        // loading.present();
        this.api.postContact(newValue).subscribe(
            (response:any) => {
              console.log(response.content);
              this.loadingControl.dismiss();
                this.massage=response.content;
                this.spinner=true;
                this.presentAlert();
            },(error)=>{
            console.log('his error ', error);
            this.loadingControl.dismiss();
            this.errorMassage = error;
        });
    }
}
