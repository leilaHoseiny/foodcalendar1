import { Component, OnInit } from '@angular/core';
import {FoodService} from '../../api/food.service';
import {getFactoryOf} from '@angular/core/src/render3';
import {Router} from '@angular/router';
import {LoadingController} from '@ionic/angular';
import {errorHandler} from '@angular/platform-browser/src/browser';
import {error} from '@angular/compiler/src/util';

@Component({
  selector: 'app-week',
  templateUrl: './week.page.html',
  styleUrls: ['./week.page.scss'],
})
export class WeekPage implements OnInit {

  public foods: any=[];
    mountNames: string[] = [
        " فروردین ",
        "اردیبهشت",
        "خرداد",
        "تیر",
        "مرداد",
        "شهریور",
        "مهر",
        "آبان",
        "آذر",
        "دی",
        "بهمن",
        "اسفند"];

  private errorMassage: any;

  constructor(private api:FoodService,private router:Router, public loadingC: LoadingController) {this.getFoods() }

  ngOnInit() {}

  async presentLoading() {
     let loading = await this.loadingC.create({
      message: 'لطفا منتظر بمانید...', // For me 'content' does not exist, it's 'message' instead
    });

    // noinspection TypeScriptUnresolvedFunction
    return await loading.present();
  }

  toPersian(mount) {
       return this.mountNames[Math.floor(mount - 1)];
      // console.log(M);
  }

  getFoods()
  {
    this.presentLoading();

    this.api.getWeekFood().subscribe((res:any)=>{
      this.loadingC.dismiss();
      this.foods= res;
    },(error)=>{
      console.log('his error ', error);
      this.loadingC.dismiss();
      this.errorMassage = error;
    })
  }

  getDetail(month,day)
  {
      this.router.navigateByUrl('menu/calendar/' + month +'/' + day );
  }
}
