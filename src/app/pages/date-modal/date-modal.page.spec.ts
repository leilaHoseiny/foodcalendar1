import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateModalPage } from './date-modal.page';

describe('DateModalPage', () => {
  let component: DateModalPage;
  let fixture: ComponentFixture<DateModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
