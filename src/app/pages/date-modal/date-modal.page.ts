import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-date-modal',
  templateUrl: './date-modal.page.html',
  styleUrls: ['./date-modal.page.scss'],
})
export class DateModalPage implements OnInit {

  constructor( public modalController: ModalController) {  }

  ngOnInit() {
  }

    // async openModal() {
    //     const modal = await this.modalController.create({
    //         // component: ExampleModalPage,
    //         componentProps: {
    //             "paramID": 123,
    //             "paramTitle": "Test Title"
    //         }
    //     });
    //
    //     // noinspection TypeScriptUnresolvedFunction
    //     modal.onDidDismiss().then((dataReturned) => {
    //         if (dataReturned !== null) {
    //             // noinspection TypeScriptUnresolvedVariable
    //             this.dataReturned = dataReturned.data;
    //             //alert('Modal Sent Data :'+ dataReturned);
    //         }
    //     });
    //
    //     // noinspection TypeScriptUnresolvedFunction
    //     return await modal.present();
    // }
}
