import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MenuPage } from './menu.page';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/menu/home',
        pathMatch: 'full'
    },
    {
        path: '',
        component: MenuPage,
        children: [
            {
                path: 'home',
                loadChildren: '../home/home.module#HomePageModule'
            },
            {
                path: 'calendar/:month/:day',
                loadChildren: '../home/home.module#HomePageModule'
            },
            {
                path: 'home',
                loadChildren: '../home/home.module#HomePageModule'
            },
            {
                path: 'food/:foodid',
                loadChildren: '../home/home.module#HomePageModule'
            },
            {
                path: 'about',
                loadChildren: '../about/about.module#AboutPageModule'
            },
            {
                path: 'contact',
                loadChildren: '../contact/contact.module#ContactPageModule'
            },
            { path: 'cooking/:id', loadChildren: '../home/cooking/cooking.module#CookingPageModule' },
            { path: 'week', loadChildren: '../week/week.module#WeekPageModule' },
            { path: 'search', loadChildren: '../search/search.module#SearchPageModule' },
        ]
    }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
