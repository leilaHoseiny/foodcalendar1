import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

    pages = [
        {
            title: 'امروز',
            url: '/menu/home',
            icon: 'home'
        },
        {
            title: 'غذاهای هفتگی',
            url: '/menu/week',
            icon: 'calendar'
        },
        {
            title: 'درباره ما',
            url: '/menu/about',
            icon: 'paper'
        },
        {
            title: 'تماس با ما',
            url: '/menu/contact',
            icon: 'call'
        },
        {
            title: 'جست وجو',
            url: '/menu/search',
            icon: 'search'
        }
    ];
  constructor() { }

  ngOnInit() {}

}
