import { Component, OnInit } from '@angular/core';
import {NavController} from '@ionic/angular';
import {FoodService} from '../../api/food.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  name:string = 'پلو';
  foods:any = [];
  constructor(public navCtr:NavController, private api: FoodService,private router:Router) {
    // this.getFood(this.name);
  }

  ngOnInit() {}

  getFood(name)
  {
      if (!name || name.length<2)
        this.foods = null;
    else {
          this.api.getFoodName(name).subscribe(res=>{
              console.log(res);
              this.foods = res;
          })
    }
  }

  foodName(event)
  {
    const foodVal = event.target.value;
    console.log(foodVal);
      this.getFood(foodVal);
  }

  getThisFood(id)
  {
      this.router.navigateByUrl('menu/food/' + id  );
  }
}
