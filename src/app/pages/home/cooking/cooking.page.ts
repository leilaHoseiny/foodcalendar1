import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FoodService} from '../../../api/food.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-cooking',
  templateUrl: './cooking.page.html',
  styleUrls: ['./cooking.page.scss'],
})
export class CookingPage implements OnInit {
     data: any;
    public id: any ;
    public detailsLoading :boolean = false;
    foodTitle: any;
    foodName: any;
    message: string;
    link: string;
  subject: string;


  constructor(
      private activRout : ActivatedRoute,
      private api: FoodService,
      private socialSharing: SocialSharing,
  ) {
    this.id = activRout.snapshot.paramMap.get('id');
    console.log('newsid', this.id);
    //   this.file.writeFile(this.file.dataDirectory, 'test.json', 'hello,world,', {replace: true})
    //     .then(_ => console.log('Directory exists'))
    //     .catch(err => console.log('Directory doesn\'t exist'));
    // }
  }
  //
  // async resolveLocalFile() {
  //   return this.file.copyFile(`${this.file.applicationDirectory}www/assets/imgs/`, 'academy.jpg', this.file.cacheDirectory, `${new Date().getTime()}.jpg`);
  // }


  ngOnInit(){
      this.goToCookie();
  }

  goToCookie()
  {
        this.detailsLoading=true;
        return this.api.getReciepe(this.id).subscribe((res: any) => {
          this.detailsLoading=false;
          console.log(res);
          this.foodName = res.fg_name;
          this.foodTitle = res.food_title;
          this.data = res.recipe;
          this.data = JSON.stringify(this.data);
          let allData = JSON.parse(this.data );
          this.data = allData.replace(/\\/g, '');

          // this.data = this.data.replace(/<[^>]+>/g, '');
          // this.data = this.data.;

           // this.data = document.body.textContent ;
          // var html = parseHTML(this.data);
          // var body = $(html).text();
          // var json = $.parseJSON(body);
          console.log(this.data);
          console.log(this.data.toString());
        },(error)=>{
            console.log( error);
            this.detailsLoading=false;
        });
    }

  share()
  {
    this.socialSharing.share(this.data).then((res:any)=>{
      console.log('share' , res);
    }).catch(()=>{});
  }
}
