// <reference path="../../../../plugins/cordova-plugin-inappbrowser/types/index.d.ts"/>
import {Component, Inject, OnInit} from '@angular/core';
import {FoodService} from '../../api/food.service';
import * as moment from 'jalali-moment';
import { LoadingController, NavController, Platform} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {SocialSharing} from '@ionic-native/social-sharing/ngx';
import {TransformnumbersService} from '../../services/transformnumbers.service';
import { File } from '@ionic-native/file/ngx';
import {environment} from '../../../environments/environment';
import {DOCUMENT} from '@angular/common';

// noinspection TsLint
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
    animations: [
        trigger('calendar',[
            state('visible',style({
                opacity: 1,
                display:'block',
            })),
            state('invisible' ,style({
                opacity: 0,
                display:'none',
            })),
            transition('*=>*' ,animate('.5s ease'))
        ])
    ],
})

export class HomePage implements OnInit {
    foods: any;
    public date;
    public mount: any ;
    private day: any;
    private M: string;
    private id: any;
    selectedDate: string = '';
    selectedDates: string[] = [];
    hideMe:boolean =true;
    visibleState = 'invisible';
    today:boolean = true;
    errorMassage :string;
    textColorPrimary:string;
    textColorSecondary:string;
    backColorPrimary:string;
    backColorSecondary:string;
    text = 'Check out the Ionic Academy!';
    url = 'https://ionicacademy.com';

    montNames: string[] = [
        " فروردین ",
        "اردیبهشت",
        "خرداد",
        "تیر",
        "مرداد",
        "شهریور",
        "مهر",
        "آبان",
        "آذر",
        "دی",
        "بهمن",
        "اسفند"];

    constructor(
      public food: FoodService,
      private navController: NavController,
      private router: Router ,
      public loadingCtrl: LoadingController,
      public activeRout: ActivatedRoute,
      private socialSharing: SocialSharing,
      public platform: Platform,
      private file: File,
      @Inject(DOCUMENT) private document: Document
    ) {
        if (activeRout.snapshot.params['month'] && activeRout.snapshot.params['day'] )
        {
            this.mount = this.activeRout.snapshot.paramMap.get('month');
            console.log(this.mount);
            this.day = this.activeRout.snapshot.paramMap.get('day');
            this.today = false;
            this.getDate();
        }else if(activeRout.snapshot.params['foodid']){
            let id= activeRout.snapshot.params['foodid'];
            this.presentLoading().then(()=>{
              this.food.getFoodId(id).subscribe((res:any ) =>{
                this.loadingCtrl.dismiss();
                console.log(res);
                let search :any = res.calendar;
                this.foods = search;
                this.mount = search.calendar.month;
                this.id = this.foods.food.id;
                this.toPersian();
                this.today =false;
              },(error)=>{
                console.log('his error ', error);
                this.dismissLoading();
                this.errorMassage = error;
              });
            });
        }
        else
        {
            this.date =  moment().locale('fa');
            this.mount= this.date.format( 'M');
            this.day= this.date.format( 'D');
            this.getDate();
        }
    }

  async shareTwitter() {
    // Either URL or Image
    this.socialSharing.shareViaTwitter(null, null, this.url).then(() => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }

  async shareWhatsApp() {
    // Text + Image or URL works
    this.socialSharing.shareViaWhatsApp(this.text, null, this.url).then(() => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }

  async resolveLocalFile() {
    return this.file.copyFile(`${this.file.applicationDirectory}www/assets/imgs/`, 'academy.jpg', this.file.cacheDirectory, `${new Date().getTime()}.jpg`);
  }

  removeTempFile(name) {
    this.file.removeFile(this.file.cacheDirectory, name);
  }

  // async shareEmail() {
  //   let file = await this.resolveLocalFile();
  //
  //   this.socialSharing.shareViaEmail(this.text, 'My custom subject', ['saimon@devdactic.com'], null, null, file.nativeURL).then(() => {
  //     this.removeTempFile(file.name);
  //   }).catch((e) => {
  //     // Error!
  //   });
  // }
  //
  // async shareFacebook() {
  //   let file = await this.resolveLocalFile();
  //
  //   // Image or URL works
  //   this.socialSharing.shareViaFacebook(null, file.nativeURL, null).then(() => {
  //     this.removeTempFile(file.name);
  //   }).catch((e) => {
  //     // Error!
  //   });
  // }

    ngOnInit() {}

    show()
    {
        this.hideMe =false;
    }

    toPersian() {
        this.M  = this.montNames[Math.floor(this.mount - 1)];
        console.log(this.M);
    }

    onSelectDate(event: string) {
        this.selectedDate = TransformnumbersService.toNormal(event);

        console.log(this.selectedDate);
        this.date = moment.from(this.selectedDate,'fa', 'YYYY/MM/DD');
        console.log(this.date);
        this.mount =this.date.format('M');
        this.day = this.date.format('D');
        console.log(this.mount,this.day);
        this.getDate();

    }

  async presentLoading() {
      let loading = await this.loadingCtrl.create({
           message:  'لطفا منتظر بمانید...', // For me 'content' does not exist, it's 'message' instead
      });
      // noinspection TypeScriptUnresolvedFunction
      return await loading.present();
  }

  async dismissLoading() {
    await this.loadingCtrl.dismiss();
  }

  // noinspection JSAnnotator
  getDate() {
    this.mount =TransformnumbersService.toNormal(this.mount);
    this.day = TransformnumbersService.toNormal(this.day);
    this.presentLoading().then(()=>{
      this.food.getApiFood(this.mount , this.day).subscribe((res: any) => {
        this.dismissLoading();
        console.log(res);
        this.foods = res.calendar;
        this.id= this.foods.food.id;
        this.backColorPrimary= res.primary_color;
        this.backColorSecondary= res.secondary_color;
        this.textColorPrimary = res.primary_text_color;
        this.textColorSecondary = res.secondary_text_color;
        this.setColor();
        this.toPersian();
        // this.setGlobalCSS();
        console.log(this.foods);
        console.log(this.textColorPrimary);
        this.visibleState = 'invisible';
      },(error)=>{
        console.log('his error ', error);
        this.dismissLoading();
        this.errorMassage = error;
      })
    });

  }

  private setGlobalCSS(textcolor,backColor) {
    // const cssText = CSSTextGenerator(this.backColor);
    this.document.documentElement.style.cssText = this.backColorPrimary;
    // this.setGlobalCSS(cssText);
    console.log( this.document.documentElement.style.cssText );
  }

  goToCookie()
  {
      this.router.navigateByUrl('menu/cooking/' + this.id);
  }

  toggleVisible()
  {
      this.visibleState = (this.visibleState == 'visible') ? 'invisible' : 'visible';
  }

  share()
  {
    console.log(this.foods.food.food_desc);
    // this.data = JSON.stringify(this.data);
    console.log('share' , this.foods);
    let text = this.foods.food.food_title+'\n'+this.foods.food.food_desc+'\n\n'+ environment.web_url;
    this.socialSharing.share( text ,this.foods.food.food_title,this.foods.food.food_pic).then((res:any)=>{
      console.log('share' , res);
    }).catch(()=>{
      console.log('Was not shared via ');
    });
  }

   setColor()
   {
     document.documentElement.style.setProperty('--ion-color-primary', this.backColorPrimary);
     document.documentElement.style.setProperty('--ion-color-primary-contrast', this.textColorPrimary);
     document.documentElement.style.setProperty('--ion-color-secondary', this.backColorSecondary);
     document.documentElement.style.setProperty('--ion-color-secondary-contrast', this.textColorSecondary);
   }

  open()
  {
   let url=encodeURI("https://www.google.com/search?q="+this.foods.food.food_title );
    window.open( url, '_system', 'location=yes');
  }
}
