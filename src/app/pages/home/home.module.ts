import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { NgJalaliCalendarModule } from 'ng-jalali-calendar'
import { HomePage } from './home.page';
import {DpDatePickerModule} from 'ng2-jalali-date-picker';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
      NgJalaliCalendarModule,
      DpDatePickerModule
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
