import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: 'menu', pathMatch: 'full' },
    { path: 'menu', loadChildren: './pages/menu/menu.module#MenuPageModule' },
  { path: 'date-modal', loadChildren: './pages/date-modal/date-modal.module#DateModalPageModule' },
  // { path: 'search', loadChildren: './pages/search/search.module#SearchPageModule' },

  // { path: 'week', loadChildren: './pages/week/week.module#WeekPageModule' },

  // { path: 'material.persian-date.adapter.ts', loadChildren: './shared/material.persian-date.adapter.ts/material.persian-date.adapter.ts.module#Material.PersianDate.Adapter.TsPageModule' },

  // { path: 'cooking/:newsid', loadChildren: './pages/home/cooking/cooking.module#CookingPageModule' }

  // { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  // { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule' },
  // { path: 'contact', loadChildren: './pages/contact/contact.module#ContactPageModule' }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
