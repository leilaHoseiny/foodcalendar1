import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TransformnumbersService {
    static numerals = {
        persian : ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"],
        arabic  : ["٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩"],
        english : ["0","1","2","3","4","5","6","7","8","9"]
    };

    public static fromEnglish(strornum:any='', lang='persian'){
        let str:string;
        if(typeof strornum === 'number')
            str = String(strornum);
        else
            str = strornum;

        if(!str || !str.length)
            return str;

        let i, len = str.length, result = "";

        for( i = 0; i < len; i++ ){

            if(TransformnumbersService.numerals["english"].indexOf(str[i]) != -1)
                result += TransformnumbersService.numerals[lang][str[i]];
            else
                result += str[i];
        }


        return result;
    }

    static toNormal(str){

        if(!str || !str.length)
            return str;

        let num, i, len = str.length, result = "";

        for( i = 0; i < len; i++ ){
            num = TransformnumbersService.numerals["persian"].indexOf(str[i]);
            num = num != -1 ? num : TransformnumbersService.numerals["arabic"].indexOf(str[i]);
            if( num == -1 ) num = str[i];
            result += num;
        }

        return result;
    }

    static toPersian(str){
        return TransformnumbersService.fromEnglish(str, "persian");
    }

    static toArabic(str){
        return TransformnumbersService.fromEnglish(str, "arabic");
    }

    static objectToPersian(object,fields=[]){
        for (let property in object) {
            if (property != 'id' && object.hasOwnProperty(property)) {
                if(!fields || fields.length <= 0  || fields.indexOf(property) !== -1){
                    console.log(property,object[property]);
                    object[property] = TransformnumbersService.toPersian(object[property]);
                }

            }
        }
        return object;
    }

    static objectToNormal(object,fields=[]){
        for (let property in object) {
            if (property != 'id' && object.hasOwnProperty(property)) {
                if(!fields || fields.length <= 0  || fields.indexOf(property) !== -1){
                    console.log(property,object[property]);
                    object[property] = TransformnumbersService.toNormal(object[property]);
                }

            }
        }
        return object;
    }

    public toPersian(str){
        return TransformnumbersService.toPersian(str);
    }

    public toArabic(str){
        return TransformnumbersService.toArabic(str);
    }

    public objectToPersian(object,fields=[]){
        return TransformnumbersService.objectToPersian(object,fields);
    }

    public objectToNormal(object,fields=[]){
        return TransformnumbersService.objectToNormal(object,fields);
    }

}
