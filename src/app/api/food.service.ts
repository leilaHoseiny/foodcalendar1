import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable,throwError} from 'rxjs';
import * as moment from 'jalali-moment';
import { LoadingController, Platform, ToastController} from '@ionic/angular';
import  'rxjs/add/observable/throw';
import {catchError} from 'rxjs/internal/operators';

@Injectable({
    providedIn: 'root'
})
export class FoodService {

    apiUrl = 'http://foodcalendar.ir/api/';
    public date = moment().locale('fa');
    private mount: any = this.date.format( 'M');
    private m: moment.Moment;
    private loading: any;

    // noinspection TsLint
  constructor(public platform:Platform,
    private http: HttpClient,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
   ){}

  getApiFood (mount, day): Observable<any>  {

    const headers = new HttpHeaders({
        'Content-Type': 'application/json',
    });

    return this.http.get(this.apiUrl + 'calendar/food/' + mount + '/' + day, {headers: headers}).pipe(
        catchError(this.errorHandler));
  }

    getReciepe(id) {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
        });
        console.log(id);
        return this.http.get(this.apiUrl + 'recipe_of_food/' + id, {headers: headers}).pipe(catchError(this.errorHandler));
    }

    getWeekFood()
    {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
        });

        return this.http.get(this.apiUrl + 'week/', {headers: headers}).pipe(catchError(this.errorHandler));
    }

    getAbout()
    {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
        });

        return this.http.get(this.apiUrl + 'about', {headers: headers}).pipe(catchError(this.errorHandler));
    }

    postContact(getValue)
    {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
        });

        return this.http.post(this.apiUrl + 'contact', getValue, {headers: headers}).pipe(catchError(this.errorHandler));
    }

    getFoodName(foodName)
    {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
        });

        return this.http.get(this.apiUrl + 'search_food/' + foodName, {headers: headers}).pipe(catchError(this.errorHandler));
    }

    getFoodId(foodId)
    {
            const headers = new HttpHeaders({
                'Content-Type': 'application/json',
            });

            return this.http.get(this.apiUrl + 'food/' + foodId, {headers: headers}).pipe(catchError(this.errorHandler));
        }

    postNotification(data){

      const headers = new HttpHeaders({
        'Content-Type': 'application/json',
      });

      return this.http.post(this.apiUrl+'push_register/', data, {headers: headers}).pipe(catchError(this.errorHandler));
    }

   errorHandler(errorResponse: HttpErrorResponse)
    {
      if (errorResponse.error instanceof ErrorEvent)
      {
          console.error('Client Side Error: ' );
          alert('خطای سمت کاربر...');
          return throwError(errorResponse.message);
      }else
      {
          alert('خطای سمت سرور...');
          console.error('Server Side Error:' );
          return errorResponse.message;
      }
      return throwError('this is problem with the service .we are notified .Please try again later ');
  }
}
